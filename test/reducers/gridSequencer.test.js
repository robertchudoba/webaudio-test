import reducer from '../../src/reducers/gridSequencer';
import { fromJS } from 'immutable';
import {
    addSequencer,
    removeSequencer,
    selectSequencer,
    toggleStart,
    toggleWrapMode,
    setDirection,
    setInterval,
    playSound,
    setWidth,
    setHeight
} from '../../src/actions/gridSequencerActions';

describe('GridSequencerReducer', () => {
});
