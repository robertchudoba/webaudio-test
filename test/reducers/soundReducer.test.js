import { fromJS } from 'immutable';
import {
    addSound,
    removeSound,
    setParam
} from '../../src/actions/soundActions';
import soundReducer from '../../src/reducers/soundReducer';

describe('soundReducer', () => {
    it('should have an initial state without any sounds', () => {
        soundReducer(undefined, {type:''}).should.equal(fromJS([]));
    })
    it('should add a sampler', () => {
        let state = soundReducer(undefined, addSound('sampler'));
        state.size.should.equal(1);
        let sound = state.get(0);
        sound.get('id').should.be.a('symbol');
        sound.should.have.property('type', 'sampler');
        sound.should.have.property('volume', 1.0);
        sound.should.have.property('file', '');
    });
    it('should remove sounds', () => {
        let init = fromJS([
            { type: 'sampler', rate: 1 },
            { type: 'sampler', rate: 2 },
            { type: 'sampler', rate: 3 }
        ]);
        soundReducer(init, removeSound(1)).should.equal(fromJS([
            { type: 'sampler', rate: 1 },
            { type: 'sampler', rate: 3 }
        ]));
    });
    it('should change sound parameters', () => {
        let init = fromJS([
            { file: '', volume: 0 }
        ]);
        soundReducer(init, setParam({
            index: 0,
            param: { name: 'file', value: 'test.wav'}
        })).should.equal(fromJS([{ file: 'test.wav', volume: 0}]));
    });
});
