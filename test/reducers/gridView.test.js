import reducer from '../../src/reducers/gridView';
import { fromJS } from 'immutable';
import { setWidth, setHeight } from '../../src/actions/gridSequencerActions';
import { setLED } from '../../src/actions/gridViewActions';

describe('GridReducer', () => {
    it('should initialize the LEDs when setting the grid size', () => {
        let state = reducer(undefined,setWidth(1));
        state.should.equal(fromJS({
            width: 1,
            height: 8,
            LEDs: [0, 0, 0, 0 ,0, 0, 0, 0]
        }));
        state = reducer(state, setHeight(1));
        state.should.equal(fromJS({
            width: 1,
            height: 1,
            LEDs: [0]
        }));
    });

    describe('with a 2x2 grid', () => {
        let state;
        beforeEach(() => {
            state = reducer(reducer(undefined, setHeight(2)),setWidth(2));
        });
        it('should have a 2x2 grid of unlit leds', () => {
            state.should.equal(fromJS({
                width: 2,
                height: 2,
                LEDs: [0, 0, 0, 0]
            }));
        })
        it('should light the LED', () => {
            state = reducer(state, setLED({x: 1, y: 1, value: 2}));
            state.should.equal(fromJS({
                width: 2,
                height: 2,
                LEDs: [0, 0, 0, 2]
            }));
            state = reducer(state, setLED({x: 1, y: 1}));
            state.should.equal(fromJS({
                width: 2,
                height: 2,
                LEDs: [0, 0, 0, 0]
            }));
        });
        it('should do nothing if you light an LED that\'s not in range', () => {
            state = reducer(state, setLED({x: 3, y: 3, value: 2}));
            state.should.equal(fromJS({
                width: 2,
                height: 2,
                LEDs: [0, 0, 0, 0]
            }));
        });
    });
});
