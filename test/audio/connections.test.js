import { Inlet, Outlet } from '../../src/audio/connections';

describe('Inlet', () => {
    let inlet;
    beforeEach(() => {
        inlet = new Inlet('test',5);
    });
    it('should have a default value', () => {
        inlet.value.should.equal(5);
    });
    it('should let you subscribe to changes', () => {
        let value = 0;
        inlet.subscribe((v) => value = v);
        inlet.value = 10;
        value.should.equal(10);
    });
});

describe('Connecting Inlet and Outlet', () => {
    let inlet, outlet;
    beforeEach(() => {
        inlet = new Inlet('inlet',0);
        outlet = new Outlet('outlet');
        outlet.connect(inlet);
    });
    it('send the value to the inlet', () => {
        outlet.value = 10;
        inlet.value.should.equal(10);
        outlet.value = 5;
        inlet.value.should.equal(5);
    });
    it('should disconnect from the inlet', () => {
        outlet.disconnect(inlet);
        outlet.value = 100;
        inlet.value.should.equal(0);
    });
});
