import GridSequencer, { WrapModes } from '../../src/audio/gridSequencer';
import { Inlet, Outlet }  from '../../src/audio/connections';

describe('GridSequencer', () => {
    let sequencer;
    beforeEach(() => {
        sequencer = new GridSequencer();
    });

    it('should have a trigger input', () => {
        sequencer.ins.trigger.should.be.instanceof(Inlet);
    });

    it('should have a stepValue output', () => {
        sequencer.outs.stepValue.should.be.instanceof(Outlet);
    });

    describe('when setting the size', () => {
        beforeEach(() => {
            sequencer.ins.width.value = 4;
            sequencer.ins.height.value = 4;
        });

        it('should output the current step value when triggered', () => {
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(0);
        });

        it('should move along the direction when triggered', () => {
            sequencer.ins.direction.value = [1,0];
            sequencer.ins.position.value = [0,0];
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(1);
            sequencer.ins.direction.value = [1,1];
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(6);
            sequencer.ins.direction.value = [-1,-1];
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(1);
        });

        it('should reverse the direction if wrapmode is "bounce"', () => {
            sequencer.ins.direction.value = [1,1];
            sequencer.ins.wrapMode.value = WrapModes.bounce;
            sequencer.ins.position.value = [3,3];
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(10);
        });
        it('should start from the beginning if wrapmode is "reset"', () => {
            sequencer.ins.direction.value = [1,1];
            sequencer.ins.wrapMode.value = WrapModes.reset;
            sequencer.ins.position.value = [3,3];
            sequencer.ins.trigger.value = 1;
            sequencer.outs.stepValue.value.should.equal(0);
        });
    });

});
