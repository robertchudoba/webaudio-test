import Node from '../../src/audio/node';
import { Inlet, Outlet } from '../../src/audio/connections';

describe('Node', () => {
    let node;
    beforeEach(() => {
        node = new Node([
            { name: 'input', defaultValue: 0 }
        ],[
            { name: 'output' }
        ]);
    });
    it('should have inputs and outputs', () => {
        node.ins.input.should.be.instanceof(Inlet);
        node.outs.output.should.be.instanceof(Outlet);
    });
});
