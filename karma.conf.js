var path = require('path');

module.exports = function(config) {
    config.set({

        files: [
            'test/tests.bundle.js'
        ],

        frameworks: ['chai-immutable', 'chai', 'mocha'],

        preprocessors: {
            'test/tests.bundle.js': ['webpack', 'sourcemap']
        },

        webpack: {
            context: path.resolve(__dirname, './test'),
            entry: './tests.bundle.js',
            devtool: 'inline-source-map',
            resolve: {
                extensions: ['', '.js', '.jsx']
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/
                    }
                ]
            }
        },

        webpackMiddleware: {
            noInfo: true
        },

        plugins: [
              require('karma-webpack'),
              require('karma-mocha'),
              require('karma-chai'),
              require('karma-chai-immutable'),
              require('karma-sourcemap-loader'),
              require('karma-phantomjs-launcher')
        ],

        browsers: ['PhantomJS']
    });
};
