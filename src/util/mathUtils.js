export function clamp(value, minimum = 0, maximum = 1) {
    let result = value;
    result = result > maximum ? maximum : result;
    result = result < minimum ? minimum : result;
    return result;
}

export function lerp(value, minimum, maximum) {
    let result = clamp(value);
    return (maximum - minimum) * result + minimum;
}

export function normalize(value, minimum, maximum) {
    let result = clamp(value, minimum, maximum);
    return result / (maximum - minimum);
}
