import './styles/main.css';
import 'font-awesome/css/font-awesome.css';

import React from 'react';
import { render } from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import gridSequencerReducer from './reducers/gridSequencer';
import gridViewReducer from './reducers/gridView';
import masterClockReducer from './reducers/masterClock';
import soundReducer from './reducers/soundReducer';
import audioGraph from './middleware/audioGraph';
import App from './components/app';

let store = createStore(
    combineReducers({
        'gridSequencer': gridSequencerReducer,
        'gridView': gridViewReducer,
        'masterClock': masterClockReducer,
        'sounds': soundReducer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(audioGraph(store));

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('render-target')
);
