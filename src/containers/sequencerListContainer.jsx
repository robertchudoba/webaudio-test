import { connect } from 'react-redux';
import {
    addSequencer,
    removeSequencer,
    selectSequencer,
    toggleSequencerStart,
    toggleWrapMode,
    setDirection,
    setInterval
} from '../actions/gridSequencerActions';
import SequencerControlList from '../components/sequencerControlList';


const mapStateToProps = (state) => ({
    sequencers: state.gridSequencer.get('sequencers').toJS()
});

const mapDispatchToProps = (dispatch) => ({
    onAddSequencer: () => dispatch(addSequencer()),
    onRemoveSequencer: (i) => dispatch(removeSequencer(i)),
    onSelectSequencer: (i) => dispatch(selectSequencer(i)),
    onToggleStart: (i) => dispatch(toggleSequencerStart(i)),
    onToggleWrapMode: (i) => dispatch(toggleWrapMode(i)),
    onDirectionChanged: (index, direction) => dispatch(setDirection({
        index,
        direction
    })),
    onIntervalChanged: (index, interval) => dispatch(setInterval({
        index,
        interval
    }))
});

const SequencerListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SequencerControlList);

export default SequencerListContainer;
