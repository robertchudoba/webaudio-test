import { connect } from 'react-redux';
import { setBPM, toggleStart } from '../actions/masterClockActions';
import MasterClockComponent from '../components/masterClockComponent';

const mapStateToProps = (state) => ({
    bpm: state.masterClock.get('bpm'),
    running: state.masterClock.get('running')
});

const mapDispatchToProps = (dispatch) => ({
    onToggleStart: () => dispatch(toggleStart()),
    onBPMChanged: (bpm) => dispatch(setBPM(bpm))
});

const MasterClockContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MasterClockComponent);

export default MasterClockContainer;
