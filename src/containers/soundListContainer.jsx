import { connect } from 'react-redux';
import { addSound, removeSound, setParam } from '../actions/soundActions';
import SoundListComponent from '../components/soundListComponent';

const mapStateToProps = (state) => ({
    sounds: state.sounds.toJS()
});

const mapDispatchToProps = (dispatch) => ({
    onAddSound: () => dispatch(addSound('sampler')),
    onRemoveSound: (k) => dispatch(removeSound(k)),
    onParamChanged: (k, param) => dispatch(setParam({index: k, param}))
});

const SoundListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SoundListComponent);

export default SoundListContainer;
