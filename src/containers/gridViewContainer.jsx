import { connect } from 'react-redux';
import {
    playSound,
    setWidth,
    setHeight
} from '../actions/gridSequencerActions';
import GridView from '../components/gridView';

const mapStateToProps = (state) => ({
    width: state.gridView.get('width'),
    height: state.gridView.get('height'),
    LEDState: state.gridView.get('LEDs').toJS()
});

const mapDispatchToProps = (dispatch) => ({
    onFieldClicked: (i) => dispatch(playSound(i)),
    onWidthChanged: (i) => dispatch(setWidth(i)),
    onHeightChanged: (i) => dispatch(setHeight(i))
});

const GridViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(GridView);

export default GridViewContainer;
