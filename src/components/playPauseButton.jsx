import React, { PropTypes } from 'react';

const PlayPauseButton = ({running, onClick}) => (
    <button
        onClick={onClick}
    >{running ? 'stop' : 'start'}</button>
);

PlayPauseButton.propTypes = {
    running: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
}

export default PlayPauseButton;
