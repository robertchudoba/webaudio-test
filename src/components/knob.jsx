import React, { Component, PropTypes } from 'react';
import { lerp, clamp, normalize } from '../util/mathUtils.js';

class Knob extends Component {

    static propTypes = {
        param: PropTypes.object.isRequired,
        minValue: PropTypes.number,
        maxValue: PropTypes.number
    }

    static defaultProps = {
        minValue: 0,
        maxValue: 1
    }

    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };
        this.minAngle = -180;
        this.maxAngle = 180;
        this.rotating = false;
        this.startRotatePos = 0;
        this.endRotate = this.endRotate.bind(this);
        this.doRotate = this.doRotate.bind(this);
    }

    componentDidMount() {
        this.setState({value: normalize(
            this.props.param.value,
            this.props.minValue,
            this.props.maxValue
        )});
        document.addEventListener('mouseup', this.endRotate);
        document.addEventListener('mousemove', this.doRotate);
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.endRotate);
        document.removeEventListener('mousemove', this.doRotate);
    }

    startRotate(evt) {
        if (this.rotating) return;
        this.rotating = true;
        this.startRotatePos = evt.clientY;
    }

    endRotate() {
        this.rotating = false;
    }

    doRotate(evt) {
        if (this.rotating) {
            let distance = this.startRotatePos - evt.clientY;
            this.setState({value: clamp(distance / 100.0)});
            this.props.param.value = lerp(
                this.state.value,
                this.props.minValue,
                this.props.maxValue
            );
        }
    }

    render() {
        let angle = lerp(this.state.value, this.minAngle, this.maxAngle);
        let knobStyle = {
            transform: `rotatez(${angle}deg)`
        };
        return (
            <div className='knob-frame'
                onMouseDown={this.startRotate.bind(this)}
            >
                <div className='knob' style={knobStyle}></div>
            </div>
        );
    }
}

export default Knob;
