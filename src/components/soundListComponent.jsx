import React, { PropTypes } from 'react';
import Sound from './soundComponent';

const SoundListComponent = ({
    sounds,
    onAddSound,
    onRemoveSound,
    onParamChanged
}) => (
    <div>
        <button
            onClick={onAddSound}
        ><span className="fa fa-plus"></span>Add</button>
        <ul className="sound-list">
            {sounds.map((sound, k) => (
                <li key={k}>
                    <Sound
                        sound={sound}
                        onRemove={() => onRemoveSound(k)}
                        onParamChanged={(param) => onParamChanged(k, param)}
                    />
                </li>
            ))}
        </ul>
    </div>
);

SoundListComponent.propTypes = {
    sounds: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.symbol.isRequired,
        type: PropTypes.string.isRequired
    })).isRequired,
    onAddSound: PropTypes.func.isRequired,
    onRemoveSound: PropTypes.func.isRequired,
    onParamChanged: PropTypes.func.isRequired
};

export default SoundListComponent;
