import React, { PropTypes } from 'react';

const IntervalSelector = ({interval, onIntervalChanged}) => (
    <div className='interval-changed'>
        <input
            type='number'
            value={interval}
            min={1} max={32} step={1}
            onChange={
                (e) => onIntervalChanged(Number.parseFloat(e.target.value))
            }
        />
    </div>
);

IntervalSelector.propTypes = {
    interval: PropTypes.number.isRequired,
    onIntervalChanged: PropTypes.func.isRequired
}

export default IntervalSelector;
