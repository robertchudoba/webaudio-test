import React, { PropTypes } from 'react';

const GridView = ({
    LEDState,
    width,
    height,
    onFieldClicked,
    onWidthChanged,
    onHeightChanged
}) => (
    <div>
        <input
            value={width}
            type='number' min='1' max='32'
            onChange={(evt) => onWidthChanged(
                parseFloat(evt.target.value)
            )}
        />
        <input
            value={height}
            type='number' min='1' max='32'
            onChange={(evt) => onHeightChanged(
                parseFloat(evt.target.value)
            )}
        />
        <ul
            className='grid-view'
            style={{
                gridTemplateColumns: `repeat(${width}, 1fr)`,
                gridTemplateRows: `repeat(${height}, 1fr)`
            }}
        >{
            LEDState.map(
                (state, k) => (
                    <li key={k}
                        className={`grid-button led-color-${state}`}
                        onClick={() => onFieldClicked(k)}
                    >
                    </li>
                )
            )
        }</ul>
    </div>
);

GridView.propTypes = {
    LEDState: PropTypes.arrayOf(PropTypes.number),
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    onFieldClicked: PropTypes.func.isRequired,
    onWidthChanged: PropTypes.func.isRequired,
    onHeightChanged: PropTypes.func.isRequired
}

export default GridView;
