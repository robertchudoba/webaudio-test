import React, { PropTypes } from 'react';
import {  Directions, DirectionVectors } from '../audio/gridSequencer.js';

const DirectionClasses = {
    [Directions.upLeft]:    'fa fa-arrow-left rotate-45',
    [Directions.up]:        'fa fa-arrow-up',
    [Directions.upRight]:   'fa fa-arrow-up rotate-45',
    [Directions.left]:      'fa fa-arrow-left',
    [Directions.center]:    'fa fa-dot-circle-o',
    [Directions.right]:     'fa fa-arrow-right',
    [Directions.downLeft]:  'fa fa-arrow-down rotate-45',
    [Directions.down]:      'fa fa-arrow-down',
    [Directions.downRight]: 'fa fa-arrow-right rotate-45'
}

const DirectionSelector = ({direction, onDirectionChanged}) => (
    <div className='direction-selector'>
        {[...DirectionVectors.keys()].map((dirKey, i) => (
        <button
            key={i}
            onClick={() => onDirectionChanged(DirectionVectors.get(dirKey))}
            className={DirectionVectors.get(dirKey) === direction ? 'active' : ''}
        ><span className={DirectionClasses[dirKey]}></span></button>
        ))}
    </div>
);

DirectionSelector.propTypes = {
    direction: PropTypes.array.isRequired,
    onDirectionChanged: PropTypes.func.isRequired
};

export default DirectionSelector;
