import React, { PropTypes } from 'react';
import Sampler from './samplerComponent';

const makeSoundComponent = (sound, onParamChanged) => {
    switch(sound.type) {
        case 'sampler':
            return <Sampler {...sound} onParamChanged={onParamChanged}/>
    }
}

const SoundComponent = ({sound,onRemove, onParamChanged}) => (
    <div>
        <button
            onClick={onRemove}
        ><span className="fa fa-times"></span></button>
        {makeSoundComponent(sound, onParamChanged)}
    </div>
);

SoundComponent.propTypes = {
    sound: PropTypes.shape({
        id: PropTypes.symbol.isRequired,
        type: PropTypes.string.isRequired
    }),
    onRemove: PropTypes.func.isRequired,
    onParamChanged: PropTypes.func.isRequired
}

export default SoundComponent;
