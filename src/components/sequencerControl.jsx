import React, { PropTypes } from 'react';
import { WrapModes } from '../audio/gridSequencer.js';
import DirectionSelector from './directionSelector';
import IntervalSelector from './intervalSelector';
import PlayPauseButton from './playPauseButton';

const SequencerControl = ({
    running,
    direction,
    wrapMode,
    interval,
    colorIndex,
    onClick,
    onRemove,
    onToggleStart,
    onToggleWrapMode,
    onDirectionChanged,
    onIntervalChanged
}) => (
    <li
        onClick={onClick}
        className={`sequencer-control led-color-${colorIndex}`}
    >
        <button
            onClick={onRemove}
        ><span className="fa fa-times"></span></button>
        <PlayPauseButton onClick={onToggleStart} running={running}/>
        <button
            onClick={onToggleWrapMode}
        >{wrapMode === WrapModes.bounce ? 'bounce' : 'reset'}</button>
        <DirectionSelector
            direction={direction}
            onDirectionChanged={onDirectionChanged}
        />
        <IntervalSelector
            interval={interval}
            onIntervalChanged={onIntervalChanged}
        />
    </li>
);

SequencerControl.propTypes = {
    running: PropTypes.bool.isRequired,
    direction: PropTypes.array.isRequired,
    interval: PropTypes.number.isRequired,
    wrapMode: PropTypes.symbol.isRequired,
    colorIndex: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onToggleStart: PropTypes.func.isRequired,
    onToggleWrapMode: PropTypes.func.isRequired,
    onDirectionChanged: PropTypes.func.isRequired,
    onIntervalChanged: PropTypes.func.isRequired
};

export default SequencerControl;
