import React, { PropTypes } from 'react';
import SequencerControl from './sequencerControl';

const SequencerControlList = ({
    sequencers,
    onAddSequencer,
    onRemoveSequencer,
    onSelectSequencer,
    onToggleStart,
    onToggleWrapMode,
    onDirectionChanged,
    onIntervalChanged
}) => (
    <div>
        <button
            onClick={onAddSequencer}
        ><span className="fa fa-plus"></span>Add</button>
        <ul className='sequencer-control-list'>
            {sequencers.map((seq, i) => (
                <SequencerControl
                    key={i}
                    onClick={() => onSelectSequencer(i)}
                    onRemove={() => onRemoveSequencer(i)}
                    onToggleStart={() => onToggleStart(i)}
                    onToggleWrapMode={() => onToggleWrapMode(i)}
                    onDirectionChanged={(dir) => onDirectionChanged(i, dir)}
                    onIntervalChanged={(interval) => onIntervalChanged(i, interval)}
                    {...seq}
                />
            ))}
        </ul>
    </div>
);

SequencerControlList.propTypes = {
    onSelectSequencer: PropTypes.func.isRequired,
    onAddSequencer: PropTypes.func.isRequired,
    onRemoveSequencer: PropTypes.func.isRequired,
    onToggleStart: PropTypes.func.isRequired,
    onToggleWrapMode: PropTypes.func.isRequired,
    onDirectionChanged: PropTypes.func.isRequired,
    onIntervalChanged: PropTypes.func.isRequired,
    sequencers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.symbol.isRequired,
        running: PropTypes.bool.isRequired,
        position: PropTypes.array.isRequired,
        direction: PropTypes.array.isRequired,
        interval: PropTypes.number.isRequired,
        wrapMode: PropTypes.symbol.isRequired
    }))
};

export default SequencerControlList;
