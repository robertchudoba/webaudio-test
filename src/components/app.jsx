import React from 'react';
import GridView from '../containers/gridViewContainer';
import SequencerList from '../containers/sequencerListContainer';
import MasterClock from '../containers/masterClockContainer';
import SoundList from '../containers/soundListContainer';

const App = () => (
    <div>
        <div>
            <h2>Master Clock</h2>
            <MasterClock/>
        </div>
        <div className='main-view'>
            <GridView/>
            <div>
                <h2>Sounds</h2>
                <SoundList/>
            </div>
        </div>
        <div>
            <h2>Sequencers</h2>
            <SequencerList/>
        </div>
    </div>
);

export default App;
