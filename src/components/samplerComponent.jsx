import React, { PropTypes } from 'react';

const SamplerComponent = ({
    volume,
    file,
    onParamChanged
}) => (
    <div>
        <div className='parameter-control'>
            <label>Volume:</label>
            <input
                value={volume}
                type='number'
                min={0}
                max={1}
                step='0.01'
                onChange={(evt) => onParamChanged({
                    name: 'volume',
                    value: parseFloat(evt.target.value)
                })}
            />
        </div>
        <div className='parameter-control'>
            <label>File:</label>
            <input
                value={file}
                type='file'
                onChange={(evt) => onParamChanged({
                    name: 'file',
                    value: evt.target.value
                })}
            />
        </div>
    </div>
);

SamplerComponent.propTypes = {
    volume: PropTypes.number.isRequired,
    file: PropTypes.string.isRequired,
    onParamChanged: PropTypes.func.isRequired
};

export default SamplerComponent;
