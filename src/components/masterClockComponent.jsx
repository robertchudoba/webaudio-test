import React, { PropTypes } from 'react';
import PlayPauseButton from './playPauseButton';

const MasterClockComponent = ({
    bpm,
    running,
    onToggleStart,
    onBPMChanged
}) => (
    <div className='master-clock'>
        <PlayPauseButton running={running} onClick={onToggleStart}/>
        <div>
            <label>BPM:</label>
            <input
                type='number'
                min={1} max={999}
                value={bpm}
                onChange={(e) => onBPMChanged(parseFloat(e.target.value))}
            />
        </div>
    </div>
);

MasterClockComponent.propTypes = {
    bpm: PropTypes.number.isRequired,
    running: PropTypes.bool.isRequired,
    onToggleStart: PropTypes.func.isRequired,
    onBPMChanged: PropTypes.func.isRequired
};

export default MasterClockComponent;
