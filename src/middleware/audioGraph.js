import _ from 'lodash';
import ClockSource from '../audio/clockSource';
import ClockDivider from '../audio/clockDivider';
import GridSequencer from '../audio/gridSequencer';
import Sampler from '../audio/sampler';
import { setLED } from '../actions/gridViewActions';

const BPMToInterval = (bpm) => Math.round((60000 / bpm) / 16);

let context = new AudioContext();
let sequencers = new Map();
let sampler = new Sampler(context);
let universalClock = new ClockSource(context);
universalClock.ins.interval.value = 1;
universalClock.ins.running.value = true;

let masterClock = new ClockDivider();
universalClock.outs.trigger.connect(masterClock.ins.trigger);
masterClock.ins.ratio.value = BPMToInterval(120);
masterClock.ins.running.value = false;
sampler.ins.path.value = require('../sounds/sound1.wav');

const createSequencer = (store, context, config) => {
    let clock = new ClockDivider();
    masterClock.outs.trigger.connect(clock.ins.trigger);
    let sequencer = new GridSequencer();
    sequencer.id = Math.floor(Math.random() * 1000);
    clock.outs.trigger.connect(sequencer.ins.trigger);
    sequencer.outs.stepValue.connect(sampler.ins.trigger);
    sequencer.outs.stepValue.subject.subscribe((value) => {
        let width = store.getState().gridView.get('width');
        let x = value % width;
        let y = Math.floor(value / width);
        store.dispatch(setLED({x, y, value: config.colorIndex}));
        window.setTimeout(() => {
            store.dispatch(setLED({x, y, value: 0}));
        }, 100);
    });
    return { sequencer, clock };
};

function setIfChanged(inlet, value) {
    if (!_.isEqual(inlet.value, value)) {
        inlet.value = value;
    }
}

/*
 * Build the audio node graph. Currently a kinda fixed graph for this
 * particular application. Future refactoring should make it more configurable.
 */

const audioGraph = store => () => {

    let state = store.getState();

    setIfChanged(masterClock.ins.running, state.masterClock.get('running'))
    setIfChanged(masterClock.ins.ratio, BPMToInterval(
        state.masterClock.get('bpm')
    ));
    let seqState = state.gridSequencer;
    let gridState = state.gridView;
    let seqData = seqState.get('sequencers');

    // remove sequencers
    let newIds = seqData.map((seq) => seq.get('id')).toJS();
    let curIds = [...sequencers.keys()];
    let missing = _.difference(curIds, newIds);
    missing.forEach((id) => {
        let s = sequencers.get(id);
        s.sequencer.outs.stepValue.disconnect(sampler.ins.trigger);
        s.clock.ins.running.value = false;
        sequencers.delete(id);
    });

    // add and modify sequencers
    seqData.forEach((seq) => {
        let id = seq.get('id');
        // Add new sequencers
        if (!sequencers.has(id)) {
            sequencers.set(
                id,
                createSequencer(store, context, seq.toJS())
            );
        }
        // TODO: generalize!
        let s = sequencers.get(id);
        setIfChanged(s.sequencer.ins.width, gridState.get('width'));
        setIfChanged(s.sequencer.ins.height, gridState.get('height'));
        setIfChanged(s.sequencer.ins.wrapMode, seq.get('wrapMode'));
        setIfChanged(
            s.sequencer.ins.direction,
            seq.get('direction').toJS()
        );
        setIfChanged(
            s.sequencer.ins.position,
            seq.get('position').toJS()
        );
        setIfChanged(s.clock.ins.ratio, seq.get('interval'));
        setIfChanged(s.clock.ins.running, seq.get('running'));
    });
}

export default audioGraph;
