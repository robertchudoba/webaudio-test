import Rx from 'rxjs';
import Pausable from './pausable';

// RX based clock source.. this will eventually be refactored using the
// web audio api as a more reliable clock

export default class ClockSource extends Pausable {
    constructor() {
        super([
            { name: 'interval', defaultValue: 500 }
        ],[]);
        this.setSource(Rx.Observable.interval(this.ins.interval.value));
        this.ins.interval.subscribe((value) => {
            this.stop();
            this.setSource(Rx.Observable.interval(value));
            this.pauser.next(this.ins.running.value);
        })
    }
}
