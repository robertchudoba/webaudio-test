import { Observable, Subject } from 'rxjs';
import Node from './node';

class Pausable extends Node {
    constructor(inlets, outlets) {
        super(
            [...inlets, { name: 'running', defaultValue: false }],
            [...outlets, { name: 'trigger' }]
        );
        this.setSource(Observable.never());
        this.pauser = new Subject();
        this.pausable = this.pauser.switchMap(
            running => running ? this.source : Observable.never()
        );
        this.pausable.subscribe((v) => {
            this.outs.trigger.value = v;
        });
        this.ins.running.subscribe((running) => {
            this.pauser.next(running);
        })
    }
    setSource(source) {
        this.source = source;
    }
    start() {
        this.pauser.next(true);
    }
    stop() {
        this.pauser.next(false);
    }
}

export default Pausable;
