export default class Envelope {

    constructor(context, param, attack = 0.01, decay = 0.05) {
        this.context = context;
        this.param = param;
        this.attack = attack;
        this.decay = decay;
    }

    trigger() {
        let now = this.context.currentTime;
        this.param.cancelScheduledValues(now);
        this.param.setValueAtTime(0, now);
        this.param.linearRampToValueAtTime(1, now + this.attack);
        this.param.linearRampToValueAtTime(0, now + this.attack + this.decay);
    }

}

