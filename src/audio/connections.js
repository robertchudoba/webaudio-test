import Rx from 'rxjs';

export class Outlet {
    constructor(name) {
        this.name = name;
        this.subscriptions = {};
        this.subject = new Rx.Subject();
        this._value = null;
    }
    set value (val) {
        this._value = val;
        this.subject.next(val);
    }
    get value () {
        return this._value;
    }
    connect(inlet) {
        if (!(inlet instanceof Inlet)) {
            throw `not an Inlet ${inlet}`;
        }
        let s = this.subject.subscribe({
            next: (value) => inlet.value = value
        });
        this.subscriptions[inlet.name] = s
    }
    disconnect(inlet) {
        this.subscriptions[inlet.name].unsubscribe();
    }
}

export class AudioNodeParam extends Outlet {
    constructor(name, audioNode) {
        super(name);
        this.param = audioNode;
    }
    set value (val) { // just forward this value the the connected inlets
        for (let s in this.subscriptions) {
            this.subscriptions[s].value = val;
        }
    }
    connect(inlet) {
        if (inlet instanceof AudioInlet) {
            this.param.connect(inlet.param);
            this.subscriptions[inlet.name] = inlet.param;
        }
    }
    disconnect(inlet) {
        if (!this.subscriptions.hasOwnProperty(inlet.name)) {
            throw `no inlet with name ${inlet.name}`;
        }
        this.param.disconnect(this.subscriptions[inlet.name]);
    }
}

export class Inlet {
    constructor(name, defaultValue) {
        this.name = name;
        this._value = defaultValue;
        this.input = Rx.Observable.create((observer) => {
            this.observer = observer;
        }); // no multicast here!
    }
    subscribe(callback) {
        this.input.subscribe(callback);
    }
    set value (val) {
        this._value = val;
        this.observer && this.observer.next(val);
    }
    get value () {
        return this._value;
    }
}

export class AudioInlet extends Inlet {
    constructor(name, audioParam) {
        super(name);
        if (!(audioParam instanceof AudioParam)) {
            throw 'argument 2 must be an AudioParam';
        }
        this.param = audioParam;
    }
    set value (val) {
        this.param.value = val;
    }
}

