import Node from './node';

export const WrapModes = {
    reset: Symbol('reset'),
    bounce: Symbol('bounce')
};

export const Directions = {
    upLeft:     Symbol('up-left'),
    up:         Symbol('up'),
    upRight:    Symbol('up-right'),
    left:       Symbol('left'),
    center:     Symbol('center'),
    right:      Symbol('right'),
    downLeft:   Symbol('down-left'),
    down:       Symbol('down'),
    downRight:  Symbol('down-right')
};

export const DirectionVectors = new Map([
    [Directions.upLeft,     [-1,-1]],
    [Directions.up,         [ 0,-1]],
    [Directions.upRight,    [ 1,-1]],
    [Directions.left,       [-1, 0]],
    [Directions.center,     [ 0, 0]],
    [Directions.right,      [ 1, 0]],
    [Directions.downLeft,   [-1, 1]],
    [Directions.down,       [ 0, 1]],
    [Directions.downRight,  [ 1, 1]]
]);

const nextPosition = (position, direction, wrapMode, size) => {
    let nextP = position.map((p,i) => p + direction[i]);
    if (wrapMode === WrapModes.reset) {
        nextP = nextP.map((p,i) => {
            if (p >= size[i] || p < 0) {
                return size[i] - position[i] - 1;
            }
            return p;
        });
    }
    return nextP;
};

const nextDirection = (position, direction, wrapMode, size) => {
    if (wrapMode === WrapModes.bounce) {
        let nextP = position.map((p,i) => p + direction[i]);
        return nextP.map((p,i) => {
            if (p == size[i] || p < 0) {
                return -1 * direction[i];
            }
            return direction[i];
        });
    }
    return direction.slice();
}

const stepIndex = (x, y, size) => y * size[0] + x;

export default class GridSequencer extends Node {
    constructor() {
        super([
            { name: 'trigger', defaultValue: false },
            { name: 'reset', defaultValue: false },
            { name: 'width', defaultValue: 1 },
            { name: 'height', defaultValue: 1 },
            { name: 'position', defaultValue: [0,0] },
            { name: 'direction', defaultValue: [0,0] },
            { name: 'wrapMode', defaultValue: WrapModes.reset }
        ], [
            { name: 'stepValue' }
        ]);

        this.position = this.ins.position.value;
        this.direction = this.ins.direction.value;
        this.ins.direction.subscribe((dir) => {
            this.direction = dir;
        });
        this.ins.position.subscribe((pos) => {
            this.position = pos;
        });

        // when receiveing a trigger event, send the next step value
        this.ins.trigger.subscribe(() => {
            this.outs.stepValue.value = this.step();
        });
    }
    step() {
        let position = this.position;
        let direction = this.direction;
        let wrapMode = this.ins.wrapMode.value;
        let size = [this.ins.width.value, this.ins.height.value];
        this.direction = nextDirection(position,direction,wrapMode,size);
        this.position = nextPosition(position,this.direction,wrapMode,size);
        let [x, y] = this.position;
        return stepIndex(x, y, size);
    }
}
