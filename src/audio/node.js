import { Inlet, Outlet } from './connections';

const mapParams = (params, Klass) => params.reduce(
    (prev, current) => (
        {...prev, [current.name]: new Klass(current.name, current.defaultValue)}
    ),
    {}
);

export default class Node {
    constructor(ins, outs) {
        this.ins = mapParams(ins, Inlet);
        this.outs = mapParams(outs, Outlet);
    }
}

export function connectNode(store, audioModule, connections) {
    store.subscribe(() => {
        let state = store.getState();
        for (var input in connections) {
            if (audioModule.ins.hasOwnProperty(input)) {
                let input = audioModule.ins[input];
                let newVal = state.getIn(connections[input]);
                if (input.value != newVal) {
                    input.value = newVal;
                }
            }
        }
    });
}
