import { Observable } from 'rxjs';
import Pausable from './pausable';

class ClockDivider extends Pausable {
    constructor() {
        super([
            { name: 'trigger', defaultValue: 0 },
            { name: 'ratio', defaultValue: 1 } // 1/1 ... 10 would be 1/10
        ],[]);

        this.setSource(
            Observable
            .from(this.ins.trigger.input)
            .filter((v) => v % this.ins.ratio.value == 0)
        );
    }
}

export default ClockDivider;
