import Node from './node';

export default class SoundSelector extends Node {
    constructor(sounds) {
        super([
            { name: 'trigger', defaultValue: 0 }
        ], []);
        this.sounds = sounds;
        this.ins.trigger.subscribe((value) => {
            if (value >= 0 && value < this.sounds.length) {
                this.sounds[value].ins.trigger.value = 1;
            }
        })
    }
}
