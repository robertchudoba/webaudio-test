import Node from './node';
import Envelope from './envelope';

class Sampler extends Node {
    constructor(context) {
        super([
            { name: 'trigger', defaultValue: 0 },
            { name: 'path', defaultValue: '' }
        ],[]);
        this.context = context;
        this.buffer = this.context.createBuffer(1, 44100, 44100);
        this.playbackRate = 1;
        this.gainNode = this.context.createGain();
        this.gainNode.connect(this.context.destination);
        this.gainNode.gain.value = 0;
        this.envelope = new Envelope(this.context, this.gainNode.gain);

        this.ins.trigger.subscribe(this.playSound.bind(this));

        this.ins.path.subscribe(this.loadFile.bind(this));
    }

    playSound() {
        this.source = this.context.createBufferSource();
        this.source.buffer = this.buffer;
        this.source.playbackRate.value = this.playbackRate;
        this.source.connect(this.gainNode);
        this.source.start();
        this.envelope.decay = 0.5;
        this.envelope.trigger();
    }

    loadFile(path) {
        fetch(path)
        .then((response) => response.arrayBuffer())
        .then((audioData) => {
            this.context.decodeAudioData(audioData, (buffer) => {
                this.buffer = buffer;
            });
        });
    }

}

export default Sampler;
