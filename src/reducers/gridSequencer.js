import { fromJS, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { WrapModes } from '../audio/gridSequencer';

const init = fromJS({
    selection: null,
    sequencers: []
});

function updateInSequencer(state, seqIndex, key, value) {
    return state.setIn(['sequencers', seqIndex, key], value);
}

const selectColor = (sequencerData) => {
    let ids = sequencerData.map((seq) => seq.get('colorIndex')).sort().toJS();
    let prev = 0;
    for (let i = 0; i < ids.length; i++) {
        if (ids[i] - prev > 1) {
            return prev + 1;
        }
        prev = ids[i];
    }
    return prev + 1;
}

// TODO: test reducer!!!

const reducer = handleActions({
    ADD_SEQUENCER: (state) => state.update(
        'sequencers',
        (list) => list.push(fromJS({
            id: Symbol(),
            wrapMode: WrapModes.reset,
            direction: [0,0],
            position: [0,0],
            running: false,
            colorIndex: selectColor(state.get('sequencers')),
            interval: 1
        }))
    ),
    REMOVE_SEQUENCER: (state, action) => state.update(
        'sequencers',
        (list) => list.remove(action.payload)
    ),
    SELECT_SEQUENCER: (state, action) => state.set(
        'selection', state.getIn(['sequencers', action.payload])
    ),
    SET_DIRECTION: (state, action) => updateInSequencer(
        state,
        action.payload.index,
        'direction',
        List(action.payload.direction)
    ),
    START_SEQUENCER: (state, action) => updateInSequencer(
        state,
        action.payload,
        'running',
        true
    ),
    TOGGLE_SEQUENCER_START: (state, action) => updateInSequencer(
        state,
        action.payload,
        'running',
        !state.getIn(['sequencers',action.payload,'running'])
    ),
    TOGGLE_WRAP_MODE: (state, action) => updateInSequencer(
        state,
        action.payload,
        'wrapMode',
        state.getIn(
            ['sequencers',action.payload,'wrapMode']
        ) == WrapModes.bounce ? WrapModes.reset : WrapModes.bounce
    ),
    STOP_SEQUENCER: (state, action) => updateInSequencer(
        state,
        action.payload,
        'running',
        false
    ),
    SET_POSITION: (state, action) => updateInSequencer(
        state,
        action.payload.index,
        'position',
        List(action.payload.position)
    ),
    SET_INTERVAL: (state, action) => updateInSequencer(
        state,
        action.payload.index,
        'interval',
        action.payload.interval
    )
}, init);

export default reducer;
