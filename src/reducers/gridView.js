import { fromJS } from 'immutable';
import { handleActions } from 'redux-actions';

const INIT_WIDTH = 8;
const INIT_HEIGHT = 8;

const init = fromJS({
    width: INIT_WIDTH,
    height: INIT_HEIGHT,
    LEDs: new Array(INIT_WIDTH*INIT_HEIGHT).fill(0)
});

const reducer = handleActions({
    SET_LED: (state, action) => {
        if (
            action.payload.x >= state.get('width') ||
            action.payload.y >= state.get('height')
        ) return state;
        return state.setIn(
            ['LEDs', action.payload.y * state.get('width') + action.payload.x],
            action.payload.value || 0
        );
    },
    SET_HEIGHT: (state, action) => state.merge({
        height: action.payload,
        LEDs: new Array(action.payload * state.get('width')).fill(0)
    }),
    SET_WIDTH: (state, action) => state.merge({
        width: action.payload,
        LEDs: new Array(action.payload * state.get('height')).fill(0)
    })
}, init);

export default reducer;
