import { fromJS }  from 'immutable';
import { handleActions } from 'redux-actions';

const init = fromJS({
    bpm: 120,
    running: false
});

const reducer = handleActions({
    SET_BPM: (state, action) => state.set('bpm', action.payload),
    START: (state) => state.set('running', true),
    STOP: (state) => state.set('running', false),
    TOGGLE_START: (state) => state.set('running',!state.get('running'))
}, init);

export default reducer;
