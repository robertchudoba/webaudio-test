import { handleActions } from 'redux-actions';
import { fromJS, List } from 'immutable';

const init = new List([]);

const makeSampler = () => (fromJS({
    id: Symbol(), type: 'sampler', volume: 1.0, rate: 1.0, file: ''
}));

const makeSound = {
    sampler: makeSampler
};

const reducer = handleActions({
    ADD_SOUND: (state, action) => state.push(makeSound[action.payload]()),
    REMOVE_SOUND: (state, action) => state.remove(action.payload),
    SET_PARAM: (state, action) => state.setIn(
        [action.payload.index, action.payload.param.name],
        action.payload.param.value
    )
}, init);

export default reducer;
