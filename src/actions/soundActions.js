import { createAction } from 'redux-actions';

export const addSound = createAction('ADD_SOUND');
export const removeSound = createAction('REMOVE_SOUND');
export const assignSound = createAction('ASSIGN_SOUND');
export const setParam = createAction('SET_PARAM');
