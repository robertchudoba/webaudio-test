import { createAction } from 'redux-actions';

export const toggleStart = createAction('TOGGLE_START');
export const setBPM = createAction('SET_BPM');
export const start = createAction('START');
export const stop = createAction('STOP');
