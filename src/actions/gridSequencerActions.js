import { createAction } from 'redux-actions';

export const addSequencer = createAction('ADD_SEQUENCER');
export const removeSequencer = createAction('REMOVE_SEQUENCER');
export const selectSequencer = createAction('SELECT_SEQUENCER');
export const toggleSequencerStart = createAction('TOGGLE_SEQUENCER_START');
export const toggleWrapMode = createAction('TOGGLE_WRAP_MODE');
export const setDirection = createAction('SET_DIRECTION');
export const setInterval = createAction('SET_INTERVAL');
export const playSound = createAction('PLAY_SOUND');
export const setWidth = createAction('SET_WIDTH');
export const setHeight = createAction('SET_HEIGHT');
